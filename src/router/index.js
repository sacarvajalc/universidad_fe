import { createRouter, createWebHashHistory } from "vue-router";
import CrearEstudiante from "../components/CrearEstudiante.vue";
import ListaEstudiantes from "../components/ListaEstudiantes.vue";
import ActualizarEstudiante from "../components/ActualizarEstudiante.vue";

const routes = [
  {
    path: "/crear",
    name: "crearE",
    component: CrearEstudiante,
  },
  {
    path: "/listar",
    name: "listarE",
    component: ListaEstudiantes,
  },
  {
    path: "/actualizar",
    name: "actualizarE",
    component: ActualizarEstudiante,
    props: true,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
